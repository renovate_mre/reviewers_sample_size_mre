FROM nginx:1.22.0

# renovate: datasource=docker depName=renovate/renovate
ARG RENOVATE_VERSION=1.0.1

# renovate: datasource=docker depName=prom/prometheus
ARG PROMETHEUS_VERSION=1.0.1

# renovate: datasource=docker depName=bitnami/thanos
ARG THANOS_VERSION=0.0.1
